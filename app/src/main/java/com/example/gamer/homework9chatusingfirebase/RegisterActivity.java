package com.example.gamer.homework9chatusingfirebase;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

public class RegisterActivity extends AppCompatActivity {

    EditText edUsername,edPassword,edEmail;
    Button btnRegister;

    FirebaseAuth firebaseAuth;
    DatabaseReference firebaseDatabase;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        edUsername=findViewById(R.id.edUser);
        edPassword=findViewById(R.id.edPass);
        edEmail=findViewById(R.id.edEmail);

        btnRegister=findViewById(R.id.btnRegister);


        firebaseAuth=FirebaseAuth.getInstance();

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String txtUser=edUsername.getText().toString();
                String txtPass=edPassword.getText().toString();
                String txtEmail=edEmail.getText().toString();

                if (TextUtils.isEmpty(txtUser)||TextUtils.isEmpty(txtPass)||TextUtils.isEmpty(txtEmail)){
                    Toast.makeText(RegisterActivity.this,"Input Invalid! Try again!!!",Toast.LENGTH_SHORT).show();
                }else if (txtPass.length()<8){
                    Toast.makeText(RegisterActivity.this,"Password have to be more than 7!!!",Toast.LENGTH_SHORT).show();
                }else if (!Patterns.EMAIL_ADDRESS.matcher(txtEmail).matches()){
                    Toast.makeText(RegisterActivity.this,"Email Invalid! Try again!!!",Toast.LENGTH_SHORT).show();
                }
                else
                    register(txtUser,txtEmail,txtPass);

            }
        });

    }

    private void register(final String username,String email, String password){
        firebaseAuth.createUserWithEmailAndPassword(email,password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            FirebaseUser firebaseUser=firebaseAuth.getCurrentUser();
                            assert firebaseUser != null;
                            String userid=firebaseUser.getUid();

                            firebaseDatabase=FirebaseDatabase.getInstance().getReference("Users").child(userid);

                            HashMap<String,String> hashMap=new HashMap<>();
                            hashMap.put("id",userid);
                            hashMap.put("username",username);
                            hashMap.put("imageURL","default");

                            firebaseDatabase.setValue(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                  if (task.isSuccessful()){
                                      Intent i=new Intent(RegisterActivity.this,MainActivity.class);
                                      i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                      startActivity(i);
                                      finish();
                                  }
                                }
                            });
                       }

                       else {
                            Toast.makeText(getApplicationContext(),"You Cannot Login with this username or password",Toast.LENGTH_SHORT).show();
                        }


                    }
                });

    }



}
