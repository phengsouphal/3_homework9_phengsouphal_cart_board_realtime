package com.example.gamer.homework9chatusingfirebase.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.gamer.homework9chatusingfirebase.MessageActivity;
import com.example.gamer.homework9chatusingfirebase.R;
import com.example.gamer.homework9chatusingfirebase.model.Chat;
import com.example.gamer.homework9chatusingfirebase.model.UserData;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.List;

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.MyViewHolder> {


    public static final int MSG_TYPE_LEFT=0;
    public static final int MSG_TYPE_RIGHT=1;
    private Context context;
    private List<Chat> chats;
    String imgUrl;

    FirebaseUser firebaseUser;

    public MessageAdapter(Context context, List<Chat> chats,String imgUrl){
        this.context=context;
        this.chats=chats;
        this.imgUrl=imgUrl;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view;
        if (i==MSG_TYPE_RIGHT){
          view =LayoutInflater.from(context).inflate(R.layout.message_right,viewGroup,false);
            return new MessageAdapter.MyViewHolder(view);
        }else {
            view =LayoutInflater.from(context).inflate(R.layout.message_left,viewGroup,false);
            return new MessageAdapter.MyViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
            Chat chat=chats.get(i);

            myViewHolder.showMessage.setText(chat.getMessage());
            if (imgUrl.equals("default"))
                myViewHolder.allUserImg.setImageResource(R.mipmap.ic_launcher);
            else
                Glide.with(context).load(imgUrl ).into(myViewHolder.allUserImg);

    }

    @Override
    public int getItemCount() {
        return chats.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder{

       public TextView showMessage;
       public ImageView allUserImg;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            showMessage=itemView.findViewById(R.id.showmessage);
            allUserImg=itemView.findViewById(R.id.imgShowUser);
        }
    }

    @Override
    public int getItemViewType(int position) {
       firebaseUser=FirebaseAuth.getInstance().getCurrentUser();
       if (chats.get(position).getSender().equals(firebaseUser.getUid()))
           return MSG_TYPE_RIGHT;
       else
           return MSG_TYPE_LEFT;
    }
}
